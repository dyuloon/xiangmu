import req from '@/apis/request.js' 

// 添加库房
export const saveStorehouse = params => req('post', '/storehousemanagement', params);

// 查询库房
export const queryStorehouse = params => req('get', '/storehousemanagement', params);

// 修改库房信息
export const updateStorehouse = params => req('put', '/storehousemanagement', params);

// 删除库房
export const deleteStorehouse = params => req('delete', '/storehousemanagement/' +  params);
