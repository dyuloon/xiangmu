import req from '@/apis/request.js' 

// 添加
export const saveAPI = params => req('post', '/healthamalysis', params);

// 查询
export const queryAPI = params => req('get', '/healthamalysis', params);

// 修改
export const updateAPI = params => req('put', '/healthamalysis', params);

// 删除
export const deleteAPI = params => req('delete', '/healthamalysis/' + params);


export const getFoldLine = params => req('get', '/healthamalysis/getFoldLine', params);


