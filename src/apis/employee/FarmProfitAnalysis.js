import req from '@/apis/request.js' 

// 添加
export const savefarmprofitanalysis = params => req('post', '/farmprofitanalysis', params);

// 查询
export const queryfarmprofitanalysis = params => req('get', '/farmprofitanalysis', params);

// 修改
export const updatefarmprofitanalysis = params => req('put', '/farmprofitanalysis', params);

// 删除
export const deletefarmprofitanalysis = params => req('delete', '/farmprofitanalysis/' + params);



