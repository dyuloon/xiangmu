import req from '@/apis/request.js' 

// 添加
export const saveempployeedate = params => req('post', '/empployeedate', params);

// 查询
export const queryempployeedate = params => req('get', '/empployeedate', params);

// 修改
export const updateempployeedate = params => req('put', '/empployeedate', params);

// 删除
export const deleteempployeedate = params => req('delete', '/empployeedate/' + params);
