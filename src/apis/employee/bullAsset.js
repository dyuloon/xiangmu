import req from '@/apis/request.js' 

// 添加
export const saveAPI = params => req('post', '/bullasset', params);

// 查询
export const queryAPI = params => req('get', '/bullasset', params);

// 修改
export const updateAPI = params => req('put', '/bullasset', params);

// 删除
export const deleteAPI = params => req('delete', '/bullasset/' + params);



