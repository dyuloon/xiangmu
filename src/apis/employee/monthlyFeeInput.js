import req from '@/apis/request.js' 

// 添加
export const saveAPI = params => req('post', '/monthlyfeeinput', params);

// 查询
export const queryAPI = params => req('get', '/monthlyfeeinput', params);

// 修改
export const updateAPI = params => req('put', '/monthlyfeeinput', params);

// 删除
export const deleteAPI = params => req('delete', '/monthlyfeeinput/' + params);



