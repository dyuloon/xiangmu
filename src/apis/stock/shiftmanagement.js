import req from '@/apis/request.js' 

// 添加
export const saveAPI = params => req('post', '/shiftmanagement', params);

// 查询
export const queryAPI = params => req('get', '/shiftmanagement', params);

// 修改
export const updateAPI = params => req('put', '/shiftmanagement', params);

// 删除
export const deleteAPI = params => req('delete', '/shiftmanagement/' + params);
