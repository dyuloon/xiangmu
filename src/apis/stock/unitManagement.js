import req from '@/apis/request.js' 

// 添加奶牛
export const saveunit = params => req('post', '/unitmanagement', params);

// 查询全部奶牛
export const queryunit = params => req('get', '/unitmanagement', params);

// 修改奶牛信息
export const updateunit = params => req('put', '/unitmanagement', params);

// 删除奶牛
export const deleteunit = params => req('delete', '/unitmanagement/' + params);
