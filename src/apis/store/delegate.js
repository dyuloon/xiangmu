import req from '@/apis/request.js' 

// 添加牛舍
export const savedelegate = params => req('post', '/delegate', params);

// 查询全部牛舍
export const querydelegate = params => req('get', '/delegate', params);

// 修改牛舍信息
export const updatedelegate = params => req('put', '/delegate', params);

// 删除牛舍
export const deletedelegate = params => req('delete', '/delegate/' + params);
