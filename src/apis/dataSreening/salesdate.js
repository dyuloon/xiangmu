import req from '@/apis/request.js' 

// 添加
export const saveAPI = params => req('post', '/salesdate', params);

// 查询
export const queryAPI = params => req('get', '/salesdate', params);

// 修改
export const updateAPI = params => req('put', '/salesdate', params);

// 删除
export const deleteAPI = params => req('delete', '/salesdate/' + params);



