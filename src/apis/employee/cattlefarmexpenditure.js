import req from '@/apis/request.js' 

// 查询
export const querycattlefarmexpenditure = params => req('get', '/cattlefarmexpenditure', params);

// 修改
export const updatecattlefarmexpenditure = params => req('put', '/cattlefarmexpenditure', params);
