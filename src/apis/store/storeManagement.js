import req from '@/apis/request.js' 

// 添加牛舍
export const savestoremanagement = params => req('post', '/storemanagement', params);

// 查询全部牛舍
export const querystoremanagement = params => req('get', '/storemanagement', params);

// 修改牛舍信息
export const updatestoremanagement = params => req('put', '/storemanagement', params);

// 删除牛舍
export const deletestoremanagement = params => req('delete', '/storemanagement/' + params);
