import req from '@/apis/request.js' 

// 添加牛舍
export const saveestrouscow = params => req('post', '/estrouscow', params);

// 查询全部牛舍
export const queryestrouscow = params => req('get', '/estrouscow', params);

// 修改牛舍信息
export const updateestrouscow = params => req('put', '/estrouscow', params);

// 删除牛舍
export const deleteestrouscow = params => req('delete', '/estrouscow/' + params);
