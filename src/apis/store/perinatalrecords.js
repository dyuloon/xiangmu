import req from '@/apis/request.js' 

// 添加牛舍
export const saveperinatal = params => req('post', '/perinatalrecords', params);

// 查询全部牛舍
export const queryperinatal = params => req('get', '/perinatalrecords', params);

// 修改牛舍信息
export const updateperinatal = params => req('put', '/perinatalrecords', params);

// 删除牛舍
export const deleteperinatal = params => req('delete', '/perinatalrecords/' + params);
