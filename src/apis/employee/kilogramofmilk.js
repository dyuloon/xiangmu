import req from '@/apis/request.js' 

// 添加
export const saveAPI = params => req('post', '/kilogramofmilk', params);

// 查询
export const queryAPI = params => req('get', '/kilogramofmilk', params);

// 修改
export const updateAPI = params => req('put', '/kilogramofmilk', params);

// 删除
export const deleteAPI = params => req('delete', '/kilogramofmilk/' + params);



