import req from '@/apis/request.js' 

// 添加
export const saveAPI = params => req('post', '/transportationmanagement', params);

// 查询
export const queryAPI = params => req('get', '/transportationmanagement', params);

// 修改
export const updateAPI = params => req('put', '/transportationmanagement', params);

// 删除
export const deleteAPI = params => req('delete', '/transportationmanagement/' + params);



