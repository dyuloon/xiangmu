import req from '@/apis/request.js' 

// 查询
export const querydecisionsupport = params => req('get', '/decisionsupport', params);

// 添加
export const savedecisionsupport = params => req('post', '/decisionsupport', params);

// 修改
export const updatedecisionsupport = params => req('put', '/decisionsupport', params);

// 删除
export const deletedecisionsupport = params => req('delete', '/decisionsupport/' + params);

export const firstShow = params => req('get', '/decisionsupport/firstShow' , params);

export const thirdShow = params => req('get', '/delegate/thirdShow' , params);

export const getShowfour = params => req('get', '/scarcerecord/getShowfour' , params);
