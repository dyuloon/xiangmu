import req from '@/apis/request.js' 

// 添加牛舍
export const saveCategory = params => req('post', '/categorymanagements', params);

// 查询全部牛舍
export const queryCategory = params => req('get', '/categorymanagements', params);

// 修改牛舍信息
export const updateCategory = params => req('put', '/categorymanagements', params);

// 删除牛舍
export const deleteCategory = params => req('delete', '/categorymanagements/' + params);
