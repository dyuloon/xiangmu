import req from '@/apis/request.js' 

// 添加
export const saveAPI = params => req('post', '/goodsmanagement', params);

// 查询
export const queryAPI = params => req('get', '/goodsmanagement', params);

// 修改
export const updateAPI = params => req('put', '/goodsmanagement', params);

// 删除
export const deleteAPI = params => req('delete', '/goodsmanagement/' + params);
