import req from '@/apis/request.js' 

// 添加
export const saveAPI = params => req('post', '/estrusrecord', params);

// 查询
export const queryAPI = params => req('get', '/estrusrecord', params);

// 修改
export const updateAPI = params => req('put', '/estrusrecord', params);

// 删除
export const deleteAPI = params => req('delete', '/estrusrecord/' + params);



