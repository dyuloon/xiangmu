import req from '@/apis/request.js' 

// 添加
export const saveAPI = params => req('post', '/scarcerecord', params);

// 查询
export const queryAPI = params => req('get', '/scarcerecord', params);

// 修改
export const updateAPI = params => req('put', '/scarcerecord', params);

// 删除
export const deleteAPI = params => req('delete', '/scarcerecord/' + params);



