import req from '@/apis/request.js'


export const Storeslastmonth = params => req('get', '/salesRecords/getStoreOrders', params);

export const Hotsellinggoods = params => req('get', '/salesRecords/getSellingGoods', params);

export const Newsalestrends = params => req('get', '/salesRecords/getSaleTrend', params);

export const getNowtotalSales = params => req('get', '/salesRecords/getNowtotalSales', params);

export const getNowAlltotalSales = params => req('get', '/salesRecords/getNowAlltotalSales', params);

export const getSalesOrders = params => req('get', '/salesRecords/getSalesOrders', params);

export const getOneShow = params => req('get', 'healthamalysis/getOneShow', params);

export const getShowFive = params => req('get', '/scarcerecord/getShowFive', params);

export const getShowOne = params => req('get', '/scarcerecord/getShowOne', params);

export const getShowfour = params => req('get', '/scarcerecord/getShowfour', params);

export const thirdShow = params => req('get', '/individualmilkproduction/thirdShow', params);

export const firstShow = params => req('get', '/storemanagement/firstShow', params);

export const thirdShows = params => req('get', '/delegate/thirdShow', params);

export const twoShow = params => req('get', '/perinatalrecords/twoShow', params);

export const firstShows = params => req('get', '/empployeedate/firstShow', params);

