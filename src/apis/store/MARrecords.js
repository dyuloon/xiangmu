import req from '@/apis/request.js' 

// 添加牛舍
export const savemarrecords = params => req('post', '/marrecords', params);

// 查询全部牛舍
export const querymarrecords = params => req('get', '/marrecords', params);

// 修改牛舍信息
export const updatemarrecords = params => req('put', '/marrecords', params);

// 删除牛舍
export const deletemarrecords = params => req('delete', '/marrecords/' + params);
