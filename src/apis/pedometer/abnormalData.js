import req from '@/apis/request.js' 

// 添加牛舍
export const saveabnormaldate = params => req('post', '/abnormaldate', params);

// 查询全部牛舍
export const queryabnormaldate = params => req('get', '/abnormaldate', params);

// 修改牛舍信息
export const updateabnormaldate = params => req('put', '/abnormaldate', params);

// 删除牛舍
export const deleteabnormaldate = params => req('delete', '/abnormaldate/' + params);
