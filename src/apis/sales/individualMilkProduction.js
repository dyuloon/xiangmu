import req from '@/apis/request.js' 

// 添加
export const saveAPI = params => req('post', '/individualmilkproduction', params);

// 查询
export const queryAPI = params => req('get', '/individualmilkproduction', params);

// 修改
export const updateAPI = params => req('put', '/individualmilkproduction', params);

// 删除
export const deleteAPI = params => req('delete', '/individualmilkproduction/' + params);



