import req from '@/apis/request.js' 

// 添加牛舍
export const savebasicsettings = params => req('post', '/basicsettings', params);

// 查询全部牛舍
export const querybasicsettings = params => req('get', '/basicsettings', params);

// 修改牛舍信息
export const updatebasicsettings = params => req('put', '/basicsettings', params);

// 删除牛舍
export const deletebasicsettings = params => req('delete', '/basicsettings/' + params);
