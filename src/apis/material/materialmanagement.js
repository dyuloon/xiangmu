import req from '@/apis/request.js' 

// 添加牛舍
export const savematerialmanagement = params => req('post', '/materialmanagement', params);

// 查询全部牛舍
export const querymaterialmanagement = params => req('get', '/materialmanagement', params);

// 修改牛舍信息
export const updatematerialmanagement = params => req('put', '/materialmanagement', params);

// 删除牛舍
export const deletematerialmanagement = params => req('delete', '/materialmanagement/' + params);
