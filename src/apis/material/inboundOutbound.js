import req from '@/apis/request.js' 

// 添加
export const saveAPI = params => req('post', '/inboundoutbound', params);

// 查询
export const queryAPI = params => req('get', '/inboundoutbound', params);

// 修改
export const updateAPI = params => req('put', '/inboundoutbound', params);

// 删除
export const deleteAPI = params => req('delete', '/inboundoutbound/' + params);



