import req from '@/apis/request.js' 

// 添加
export const savecattleFarmIncome = params => req('post', '/cattlefarmincome', params);

// 查询
export const querycattleFarmIncome = params => req('get', '/cattlefarmincome', params);

// 修改
export const updatecattleFarmIncome = params => req('put', '/cattlefarmincome', params);

// 删除
export const deletecattleFarmIncome = params => req('delete', '/cattlefarmincome/' + params);



