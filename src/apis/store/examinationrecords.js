import req from '@/apis/request.js' 

// 添加牛舍
export const saveexamin = params => req('post', '/examinationrecords', params);

// 查询全部牛舍
export const queryexamin = params => req('get', '/examinationrecords', params);

// 修改牛舍信息
export const updateexamin = params => req('put', '/examinationrecords', params);

// 删除牛舍
export const deleteexamin = params => req('delete', '/examinationrecords/' + params);
