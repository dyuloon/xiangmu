import req from '@/apis/request.js' 

// 查询
export const queryfeedinganalysis = params => req('get', '/feedinganalysis', params);

// 添加
export const savefeedinganalysis = params => req('post', '/feedinganalysis', params);

// 修改
export const updatefeedinganalysis = params => req('put', '/feedinganalysis', params);

// 删除
export const deletefeedinganalysis = params => req('delete', '/feedinganalysis/' + params);
