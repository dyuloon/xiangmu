import req from '@/apis/request.js' 

// 添加库房
export const saveuntappedmilk = params => req('post', '/untappedmilk', params);

// 查询库房
export const queryuntappedmilk = params => req('get', '/untappedmilk', params);

// 修改库房信息
export const updateuntappedmilk = params => req('put', '/untappedmilk', params);

// 删除库房
export const deleteuntappedmilk = params => req('delete', '/untappedmilk/' +  params);
