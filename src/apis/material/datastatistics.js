import req from '@/apis/request.js' 

// 添加
export const saveAPI = params => req('post', '/datastatistics', params);

// 查询
export const queryAPI = params => req('get', '/datastatistics', params);

// 修改
export const updateAPI = params => req('put', '/datastatistics', params);

// 删除
export const deleteAPI = params => req('delete', '/datastatistics/' + params);
